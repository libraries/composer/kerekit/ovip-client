<?php

namespace Kerekit\OvipClient;

class Client
{
    public const ENDPOINT_PROD = 'https://www.ovip.hu/webshopAPI/';
    public const ENDPOINT_TEST = 'https://www.ovip.innovip.hu/webshopAPI/';
    public const VALID_ACTIONS = [
        'getProducts',
        'getPricelist',
        'getCategories',
        'getCategoriesPlus',
        'GetQtyDiscount',
        'getParams',
        'getParamValues',
        'getImages',
        'getStock',
        'getManufacture',
        'getStockManufacture',
        'sendOrder',
    ];

    protected string      $_authCode;
    protected string      $_ipAddress;
    protected \SoapClient $_soapClient;
    protected bool        $_testMode;
    protected string      $_userId;
    protected string      $_webshopId;

    /**
     * @throws Exception on expected errors
     * @throws \SoapFault on unexpected errors
     */
    public function __call (string $action, array $arguments)
    {
        if (!in_array ($action, self::VALID_ACTIONS)) {
            throw new \Exception ("Invalid action: '$action'.");
        }

        $params = [
            'request'    => $action,
            'user_id'    => $this->_userId,
            'signature'  => $this->_getSignature ($action),
            'webshop_id' => $this->_webshopId,
        ];
        $argCount = count ($arguments);

        // Add extra data to params if available
        if ($argCount === 1) {
            $params ['extra_data'] = $arguments [0];
        }

        // Too many arguments
        if ($argCount > 1) {
            throw new \Exception ("Expected 0 or 1 arguments, got $argCount instead.");
        }

        try {
            return $this->_soapClient->getRequest ($params);
        } catch (\SoapFault $e) {
            switch ($e->getMessage ()) {

                case 'Nincs érték.':
                    $code = Exception::NO_VALUES;
                    break;

                case 'Túl sok futás!':
                    $code = Exception::RATE_LIMIT_EXCEEDED;
                    break;

                default:
                    throw $e;
            }
            throw new Exception ($e->getMessage (), $code);
        }
    }

    public function __construct (
        string $ipAddress,
        string $userId,
        string $webshopId,
        string $authCode,
        bool   $testMode
    )
    {
        $this->_ipAddress = $ipAddress;
        $this->_userId    = $userId;
        $this->_webshopId = $webshopId;
        $this->_authCode  = $authCode;
        $url = $testMode ? self::ENDPOINT_TEST : self::ENDPOINT_PROD;
        $this->_soapClient = new \SoapClient (null, [
            'location' => $url,
            'uri'      => $url,
            'encoding' => 'UTF-8',
        ]);
    }

    protected function _getSignature (string $action): string
    {
        $parts = [
            $this->_userId,
            $this->_webshopId,
            $this->_authCode,
            $action,
            $this->_ipAddress,
        ];
        $raw = implode ('', $parts);
        return hash ('sha256', $raw);
    }
}
