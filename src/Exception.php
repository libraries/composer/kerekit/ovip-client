<?php

namespace Kerekit\OvipClient;

class Exception extends \Exception
{
    public const NO_VALUES           = 1;
    public const RATE_LIMIT_EXCEEDED = 2;
}
